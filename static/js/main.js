// select current nav element



document.addEventListener("DOMContentLoaded", function(event) {
    let nav_links = document.getElementsByClassName('nav_link');

    for (let i = 0; i < nav_links.length; i++) {
        if (nav_links[i].href === document.URL) {
            nav_links[i].className += ' active_nav';
        }
    }
});


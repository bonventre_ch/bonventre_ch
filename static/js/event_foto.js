var all_files;
var fileSend;

window.onload = function() {
    document.getElementById("files").addEventListener("change", select_files, false);
    
    fileSend = document.getElementById("fileSend");
    fileSend.onclick = function(e) {
        e.stopPropagation();
        e.preventDefault();

        if (all_files.length == 0) {
            alert("no files to upload");
        }

        for (let i=0; i<all_files.length; i++) {
            sendFile(all_files[i]);
        }
    }

}

function sendFile(file) {
    const uri = "/index.php";
    const xhr = new XMLHttpRequest();
    const fd = new FormData();

    xhr.open("POST", uri, true);
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && xhr.status == 200) {
            // alert(xhr.responseText);
        }
    };
    fd.append('myFile', file);
    xhr.send(fd);
}

function select_files(e) {
    all_files = e.target.files;
    for (var i = 0, f; f = all_files[i]; i++) {
        if (!f.type.match('image.*')) {
            continue;
        }

        if (all_files.length > 0) {
            fileSend.style.visibility = "visible";
        }

        var reader = new FileReader();
        reader.onload = (function (theFile) {
            return function (e) {
                var preview_img = document.createElement('img');
                preview_img.className = 'preview_img';
                preview_img.src = e.target.result;
                preview_img.title = theFile.name;
                document.getElementById('list').insertBefore(preview_img, null);
            };
        })(f);
        reader.readAsDataURL(f);
    }
}
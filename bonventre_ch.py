#!/usr/bin/env python

""" Setup for deployment
add env var for: application secret key, email password, backupbox_fragen_init token, gruppen_fotos token to add new album
disable debugging
check script with pyflakes "python -m pyflakes bonventre.py"

sudo pacman -S python base-devel
python -m venv env
source env/bin/activate
pip install -r requirements.txt

# Changing the db model requires deleting the old database first
source env/bin/activate
flask shell
from bonventre_ch import db
db.create_all()
exit()

python wsgi.py
"""

from flask import Flask, render_template, request, redirect, send_from_directory, flash, make_response, abort
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime, date
import os
import json
# import imghdr
from werkzeug.utils import secure_filename
import zipfile
import pathlib
from PIL import Image
from PIL.ExifTags import TAGS
from exif import Image as exifImage
from smtplib import SMTP_SSL
from email.message import EmailMessage
import base64
import uuid
from hashlib import blake2b

""" ToDo

maybe have a function for setting and removing cookies
remove most url params and use cookies with func for setting, with max_age cookies

tasks per user with cookies like how i did timers
try me bitch
import only what is needed
light/dark switch in nav
language switch in nav / all strings to const with language files (reseach)


## LinkTracker / LinkShortner
https://www.digitalocean.com/community/tutorials/how-to-make-a-url-shortener-with-flask-and-sqlite
Check if a shared link has been clicked
    
    ### must have
        - link to redirect to
        - bonventre.ch/{linkName} speaking url
        - mail on click with relevant info



## SiteTracker
Check for updates to a website with an interval and notify

    ### must have
        - link to track
        - checking interval
        - send mail upon changes detected
        - check for only one html tag's contents



## GruppeTimer
Timer to share, tracking the days until an event occurs

    ### todo

    remove old timers
    add timer to a cookie with max_age/expires as timer date + 1


    ### must have
        - countdown on website in days
        - short hash for each timer saved as primary key in db
        - share link to countdown
        - download calendar entry .icv
        - save info in cookies with TTL as big as the timer plus 1
        - load timers from cookies

    ### view

    Title           : GruppeTimer 
    text input      : name of event
    text input      : hex color
    date picker     : browser date picker on day granularity

    Title           : my Timers
    list of timers  : (XX days until TimerName) in hex color



## GruppeFoti

    go back from album without going to the bottom of the page, maybe show navbar?
    check if album about to be added already exists
    new album:
        - define how many uploaders there are going to be
        - on each upload ask if this have been all photos
            and if so check if max uploader has been reached which should send an email to all uploaders
    new upload:
        - check hash of file and look for dups
    upload to rsync storage
        https://stackoverflow.com/questions/12295551/how-to-list-all-the-folders-and-files-in-the-directory-after-connecting-through

    user select duplicate
        https://github.com/python-pillow/Pillow/issues/5786
        https://www.tutorialexample.com/python-compare-two-images-whether-same-or-not-python-pillow-tutorial/
        https://stackoverflow.com/questions/35176639/compare-images-python-pil


    Errors:
        - on multi upload ~12 internal server error appears as response on frontend

    frontend:
        - order image by name (date)
        - checkbox to download

    Python config on Webserver
        - max RAM
        - max concurrent users
        - max upload size
        - max size per img
        - min resolution?
"""

""" Application flow

GruppeFotis user
1. Get folder and token
2. Login
3. Upload 1-n photos
4. Select 1-n photos to download as a compressed archive


GruppeFotis program upload
1. Only allow photos
2. Check total size and of each photo for size < n
3. Save photos on webserver
4. For each uploaded photo check for duplicates
4a. For each dup ask user which or both to keep


GruppeFotis program upload
0. Ask user for name
1. Only allow photos
2. Check total size and of each photo for size < n
3. Save photos on webserver
3a. Rename based on exif created_date
3b. Add new metadata
4. For each uploaded photo check for duplicates
4a. Create low resolution version of duplicate and on file photo
4b. For each dup ask user which or both to keep
5. Inform user about current expiration date of the files (folder create date + 30 days)


GruppeFotis program download
1. Login
2. Select photos
3. Ask for OS
4. Create archive on FS and give it the folder name
5. Download archive
6. Delete archive

"""

# import pdb
# pdb.set_trace()


basedir = os.path.abspath(os.path.dirname(__file__))

app = Flask(__name__)
app.secret_key = "secret key"
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'instance/main.db')
app.config['MAX_CONTENT_LENGTH'] = 100 * 1024 * 1024
app.config['UPLOAD_EXTENSIONS'] = ['.jpg', '.jpeg', '.png', '.webp']
app.config['UPLOAD_PATH'] = 'uploads'
db = SQLAlchemy(app)

# app.logger.error('An error occurred')


class BackupBoxUmfrage(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    dt = db.Column(db.DateTime, default=datetime.utcnow, nullable=False)
    user_hash = db.Column(db.String(50), nullable=False)
    name = db.Column(db.String(50), nullable=False)
    vorname = db.Column(db.String(50), nullable=False)
    antworten_json = db.Column(db.String, nullable=False)

    def __repr__(self):
        return f'[{self.dt}] "{self.id}" name: {self.name}, vorname: {self.vorname}'


class UserStats(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    dt = db.Column(db.DateTime, default=datetime.utcnow, nullable=False)
    ip = db.Column(db.String, nullable=False)
    user_agent = db.Column(db.String(50), nullable=False)
    path = db.Column(db.String(50), nullable=False)
    params = db.Column(db.String(50), nullable=True)

    def __repr__(self):
        return f'[{self.dt}] "{self.id}" IP:{self.ip}, UA: {self.user_agent}, path: {self.path}, params: {self.params}'


class GruppeTimer(db.Model):
    id = db.Column(db.String, primary_key=True)
    name = db.Column(db.String, nullable=False)
    end_date = db.Column(db.DateTime, nullable=False)
    remaining_days = db.Column(db.Integer, nullable=False)
    color_hex = db.Column(db.String, nullable=False)

    def __repr__(self):
        return f'{self.id}: Timer name: {self.name}, ending date: {self.end_date}, color hex: {self.color_hex}'


class Todo(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    content = db.Column(db.String(200), nullable=False)
    date_created = db.Column(db.DateTime, default=datetime.utcnow, nullable=False)

    def __repr__(self):
        return f'Task {self.task_id}'


class GruppeFotiAlbum(db.Model):
    id = db.Column(db.Integer, primary_key=True, nullable=False)
    name = db.Column(db.String(50), nullable=False, unique=True)
    token = db.Column(db.String(50), nullable=False)
    date_created = db.Column(db.DateTime, default=datetime.utcnow, nullable=False)
    uploaders = db.Column(db.Integer, nullable=False)

    def __repr__(self):
        return f'{self.id}: token_id: {self.token_id}, ' \
               f'name: {self.name}, token: {self.token}, amount of uploaders: {self.uploaders}'


class GruppeFotiUploader(db.Model):
    id = db.Column(db.Integer, primary_key=True, nullable=False)
    album_key = db.Column(db.Integer, nullable=False)
    uploader_name = db.Column(db.String(25), nullable=False)

    def __repr__(self):
        return f'{self.id}: album_key: {self.album_key}, uploader_name: {self.uploader_name}'


def get_field(exif, field):
    for (k, v) in exif.items():
        if TAGS.get(k) == field:
            return v


def send_email(email_body_text, email_subject, email_recipients):
    email_address = 'info@bonventre.ch'
    email_password = 'testing'
    email_server = 'mail.infomaniak.com'
    email_server_port = 465

    msg = EmailMessage()
    msg['Subject'] = email_subject
    msg['From'] = email_address
    msg['To'] = email_recipients
    msg.set_content(email_body_text)

    with SMTP_SSL(email_server, email_server_port) as smtp:
        smtp.login(email_address, email_password)
        smtp.send_message(msg)


def log_error(log_msg='', flash_msg='', should_flash=False, alert_per_email=False):
    if alert_per_email:
        send_email(flash_msg, log_msg, 'leroy@bonventre.ch')
    app.logger.error(log_msg)
    if should_flash:
        flash(flash_msg, 'error')


@app.errorhandler(404)
def page_not_found(error):
    log_error(f'page not found, {error}', should_flash=False)
    return render_template('404.html'), 404


def check_input(input_var, flash_msg, page_reload):
    if str(input_var) != '':
        return str(input_var)
    else:
        log_error(f'the expected value for {input_var} is empty', flash_msg, should_flash=True)
        return redirect(page_reload)


def log_user_stats():
    dt = datetime.now()
    ip = request.access_route[0]
    user_agent = request.user_agent.string
    try:
        path, params = request.full_path.split('?')
    except ValueError:
        path = request.path
        params = None

    log = UserStats(dt=dt, ip=ip, user_agent=user_agent, path=path, params=params)
    try:
        db.session.add(log)
        db.session.commit()
    except Exception as e:
        print(e)
        log_error('logging user data failed', 'logging user data failed', should_flash=True)
        abort(404)


@app.route('/')
def index():
    """
    ToDo
    """
    log_user_stats()

    # count page views from the same IP only for different days
    visitors = dict(dict())
    data = UserStats.query.with_entities(UserStats.ip, UserStats.dt).order_by(UserStats.dt).all()
    # print(data)
    currently_latest_day = 0
    for idx, row in enumerate(data):
        if row.ip not in visitors:
            visitors[row.ip] = {idx: int(row.dt.strftime("%d"))}
            if visitors[row.ip][idx]:
                currently_latest_day = visitors[row.ip][idx]
            """
            for entry in visitors[row.ip]:
                print(entry)
                currently_latest_day = visitors[row.ip][entry]
                print(f"currently_latest_day: {currently_latest_day}")
            """

    # print(visitors)

    page_view_counter = UserStats.query.count()
    tasks = Todo.query.order_by(Todo.date_created).all()

    delete_cookie = False

    current_timers = []
    current_timer_id = ''
    today = date.today()
    all_timer = GruppeTimer.query.all()
    if len(all_timer) > 0:
        for timer in all_timer:
            current_timer_id = 'timer_id_' + timer.id
            my_timer = request.cookies.get(current_timer_id)
            if my_timer is not None:
                days_until_done = (timer.end_date.date() - today).days
                if days_until_done > 0:
                    timer.remaining_days = days_until_done
                else:
                    delete_cookie = True
                    try:
                        db.session.delete(timer)
                        db.session.commit()
                    except:
                        log_error(f'could not remove task from db with id: {id}',
                                  'Aufgabe wurde nicht entfernt!', should_flash=True)
                try:
                    db.session.commit()
                except:
                    log_error(f'could not update remaining_days in db for timer: {timer}',
                              'Timer ist fehlerhaft!', should_flash=True)
                current_timers.append(timer)

    # ToDo: fix me
    # this is not needed, only if we navigate to a specific folder
    # maybe a func since it is also used within access_album
    files = os.listdir(app.config['UPLOAD_PATH'])
    images = []
    for file in files:
        file_ext = os.path.splitext(file)[1]
        if file_ext in app.config['UPLOAD_EXTENSIONS']:
            images.append(file)

    resp = make_response(
        render_template(
            'index.html', nav_id=request.args.get('nav'),
            tasks=tasks, files=images, timers=current_timers, page_view_counter=page_view_counter
        )
    )
    if delete_cookie:
        resp.delete_cookie(current_timer_id, path='/', domain=None)
    return resp


@app.route('/create_timer', methods=['POST'])
def create_timer():
    """
    get form input
    create unique id for timer in order for them to be shareable
    add new timer to db
    make response
    add current timer with id to cookie
    """
    page_reload = '/?nav=gruppe_timer'

    input_name = check_input(request.form['event_name'], 'Das Event muss einen Namen haben!', page_reload)
    input_hex = check_input(request.form['event_hex'], 'Das Event muss eine Farbe haben!', page_reload)
    input_date = check_input(request.form['event_date'], 'Das Event muss ein Enddatum haben!', page_reload)
    my_date = datetime.strptime(input_date, '%Y-%m-%d')

    timer_uuid = base64.urlsafe_b64encode(uuid.uuid4().bytes).decode("utf-8")
    timer_uuid = str(timer_uuid).replace('=', '')

    new_timer = GruppeTimer(id=timer_uuid, name=input_name, color_hex=input_hex, end_date=my_date, remaining_days='')
    try:
        db.session.add(new_timer)
        db.session.commit()
    except:
        log_error('unable to add new timer', 'Neuer Timer konnte nicht hinzugefügt werden!', should_flash=True)
        return redirect(page_reload)

    days_until_done = (new_timer.end_date.date() - date.today()).days
    resp = make_response(redirect(page_reload))
    resp.set_cookie('timer_id_' + timer_uuid, value=input_name, max_age=60 * 60 * 24 * days_until_done,
                    expires=60 * 60 * 24 * days_until_done, path='/', domain=None, secure=None, httponly=False)
    return resp


@app.route('/access_timer/<timer_id>', methods=['GET'])
def access_timer(timer_id):
    """
    get timer with id from db
    make response
    add current timer with id to cookie
    """
    page_reload = '/?nav=gruppe_timer'

    try:
        my_timer_query = GruppeTimer.query.filter_by(id=timer_id).first()
    except:
        log_error(f'unable to find timer with id: {id}', 'Timer wurde nicht gefunden!', should_flash=True)

        return redirect(page_reload)

    days_until_done = (my_timer_query.end_date.date() - date.today()).days
    resp = make_response(redirect(page_reload))
    resp.set_cookie('timer_id_' + timer_id, value=my_timer_query.name, max_age=60 * 60 * 24 * days_until_done,
                    expires=60 * 60 * 24 * days_until_done, path='/', domain=None, secure=None, httponly=False)
    return resp


@app.route('/hide_timer/<timer_id>', methods=['GET'])
def hide_timer(timer_id):
    """
    make response
    remove current cookie for timer id
    """
    page_reload = '/?nav=gruppe_timer'

    resp = make_response(redirect(page_reload))
    resp.delete_cookie('timer_id_' + timer_id, path='/', domain=None)
    return resp


@app.route('/upload_files/<album_name>?input_token=<input_token>&input_name=<input_name>', methods=['POST'])
def upload_files(album_name, input_token, input_name):
    """
    if request method is POST
        get the send files
        loop all the files
            if the file type is allowed
                create path vars
                save file
                create new file name with original datetime
                rename file
                add exif data
        return to album
    """
    form_files = request.files.getlist('files[]')
    if len(form_files) < 1:
        log_error('no files selected to upload', 'Wähle Dateien aus, um sie hochzuladen!', should_flash=True)
        return access_album(album_name, input_token, input_name)

    for form_file in form_files:
        filename = secure_filename(form_file.filename)

        if '.' not in filename:
            log_error(f'file without extension is not allowed: {filename}',
                      f'Dateien ohne Endungen sind nicht erlaubt! {filename}', should_flash=True)
            return access_album(album_name, input_token, input_name)

        file_ext = filename.rsplit('.', 1)[1]

        if file_ext.lower() not in app.config['UPLOAD_EXTENSIONS']:
            log_error(f'file extension {file_ext} is not allowed',
                      f'Dateien mit {file_ext} sind nicht erlaubt!', should_flash=True)
            continue

        upload_dir = os.path.join(app.config['UPLOAD_PATH'], album_name)
        full_path = os.path.join(upload_dir, filename)
        form_file.save(full_path)

        image = Image.open(full_path)
        exif_data = image.getexif()
        date_created = get_field(exif_data, 'DateTime')

        datetime_object = datetime.strptime(date_created, '%Y:%m:%d %H:%M:%S')
        date_str = datetime_object.strftime('%Y%b%d_%H%M%S')

        new_full_path = \
            os.path.join(upload_dir, f'IMG_{album_name}_{date_str}{file_ext}')

        if os.path.isfile(new_full_path):
            date_str = date_str + '_' + str(datetime.now().microsecond)
            new_full_path = \
                os.path.join(upload_dir, f'IMG_{album_name}_{date_str}{file_ext}')

        if os.path.isfile(full_path):
            os.rename(full_path, new_full_path)

        # ToDo: set file access time, also change the crate date to the original
        file_orig_dates = (datetime_object.timestamp(), datetime_object.timestamp())
        os.utime(new_full_path, file_orig_dates)

        if file_ext.lower() == '.jpg':
            with open(new_full_path, 'rb') as path_file:
                img = exifImage(path_file)

            img.image_description = f'Part of {album_name}'
            img.copyright = f'{input_name} uploaded to bonventre.ch'

            with open(new_full_path, 'wb') as img_to_edit:
                img_to_edit.write(img.get_file())

    query_sub_id = GruppeFotiAlbum.query.filter_by(name=album_name).first()
    uploader_from_cookie = request.cookies.get('uploader_name')

    # check if foreign key table already has (album_id, uploader_name) row
    all_uploaders = GruppeFotiUploader.query.filter_by(album_key=query_sub_id.id,
                                                       uploader_name=uploader_from_cookie).all()
    if len(all_uploaders) > 0:
        pass
    else:
        new_uploader = GruppeFotiUploader(album_key=query_sub_id.id, uploader_name=uploader_from_cookie)
        try:
            db.session.add(new_uploader)
            db.session.commit()
        except Exception as e:
            print(e)
            log_error('unable to add new uploader', 'Neuer Benutzer konnte nicht hinzugefügt werden!',
                      should_flash=True)

        # ToDo
        # ask user if those were all their images
        # needs a new form to submit

        # if all uploaders have uploaded send email to all to notify them that the download is ready 

    return access_album(album_name, input_token, input_name)


@app.route('/img_to_display/<album_name>/<filename>')
def img_to_display(album_name, filename):
    """
    create path var
    if the file exists
        remove it
    """
    download_path = os.path.join(app.config['UPLOAD_PATH'], album_name)
    return send_from_directory(download_path, filename)


@app.route('/delete_file/<album_name>?input_token=<input_token>&input_name=<input_name>&filename=<filename>')
def delete_file(album_name, input_token, input_name, filename):
    """
    create path var
    if the file exists
        remove it
    """
    full_path = f'{app.config["UPLOAD_PATH"]}/{album_name}/{filename}'
    if os.path.exists(full_path):
        os.remove(full_path)
        return access_album(album_name, input_token, input_name)

    log_error('image to be deleted at pathnot found: {full_path}', 'Bild nicht gefunden', should_flash=True)
    return access_album(album_name, input_token, input_name)


@app.route('/access_album', methods=['POST'])
def access_album(input_album='', input_token='', input_name=''):
    """
    get form data: album, token, name
    show requested album
    """
    page_reload = '/?nav=gruppe_foti'

    input_album = check_input(request.form['album'], 'Das Album konnte nicht gefunden werden!', page_reload)
    input_token = check_input(request.form['token'], 'Das Album konnte nicht gefunden werden!', page_reload)
    input_name = check_input(request.form['name'], 'Das Album konnte nicht gefunden werden!', page_reload)

    # album is the new album's name formatted as name#5 with the amount of uploaders, token is its secret key
    if input_name == 'super-secret-token':
        try:
            input_album_name = input_album.split('#')[0]
            input_album_uploaders = input_album.split('#')[1]
        except Exception as e:
            print(e)
            log_error('trying to add new album with invalid name', 'Format ist name#5', should_flash=True)
            return redirect(page_reload)

        new_album_query = GruppeFotiAlbum.query.filter_by(name=input_album_name).first()
        my_var = check_input(request.form['album'], 'Album mit diesem Namen existiert bereits!', page_reload)

        new_album = GruppeFotiAlbum(name=input_album_name, token=input_token, uploaders=input_album_uploaders)

        try:
            db.session.add(new_album)
            db.session.commit()
        except Exception as e:
            print(e)
            log_error('could not add new album to db', 'could not add new album to db', should_flash=True)
            return redirect(page_reload)

        flash('new album added', 'error')
        return redirect(page_reload)

    try:
        current_album = GruppeFotiAlbum.query.filter_by(name=input_album).first()
    except AttributeError as e:
        log_error(f'could not find album with name in db {e}', 'Ungültige Eingabe!',
                  should_flash=True)
        return redirect(page_reload)

    if current_album.token != input_token:
        flash('Ungültige Eingabe!', 'error')
        return redirect(page_reload)
    else:
        # clean up previous zip
        zip_filename = f'{input_album}.zip'
        directory = pathlib.Path(f'{app.config["UPLOAD_PATH"]}/{input_album}')
        old_file = os.path.join(directory, zip_filename)

        if os.path.isfile(old_file):
            os.remove(old_file)

        query_album_name = f'{app.config["UPLOAD_PATH"]}/{current_album.query_album_name}'
        if not os.path.isdir(query_album_name):
            os.mkdir(query_album_name)
        files = os.listdir(query_album_name)
        images = []
        for file in files:
            file_ext = os.path.splitext(file)[1]
            if file_ext in app.config['UPLOAD_EXTENSIONS']:
                images.append(file)
        images.sort()

        resp = make_response(
            render_template('update_album.html', album_name=input_album, files=images, input_token=input_token,
                            input_name=input_name))
        resp.set_cookie('uploader_name', input_name, max_age=60 * 60 * 24 * 365 * 2)
        return resp


@app.route('/download_files/<album_name>?input_token=<input_token>', methods=['POST'])
def download_files(album_name='', input_token=''):
    """
    if sub_dir
        vars for path and zip
        remove zip if necessary
        create new zip
            iterate dir
                if file extension
                    add to archive
        return zip
    """
    if not input_token:
        return redirect('/?nav=gruppe_foti')

    zip_filename = f'{album_name}.zip'
    directory = pathlib.Path(f'{app.config["UPLOAD_PATH"]}/{album_name}')
    new_file = os.path.join(directory, zip_filename)

    if os.path.isfile(new_file):
        os.remove(new_file)
    with zipfile.ZipFile(new_file, mode='w') as archive:
        for file_path in directory.iterdir():
            file_ext = os.path.splitext(file_path)[1]
            if file_ext in app.config['UPLOAD_EXTENSIONS']:
                archive.write(file_path, arcname=file_path.name)
    return send_from_directory(directory=directory, path=zip_filename)


@app.route('/task/create', methods=['POST'])
def create_task():
    """
    if request is POST
        new task
        commit new task to db
    """
    task_content = request.form['content']
    new_task = Todo(content=task_content)

    try:
        db.session.add(new_task)
        db.session.commit()
    except Exception as e:
        print(e)
        log_error('could not add task to db ', 'Aufgabe wurde nicht hinzugefügt!', should_flash=True)

    return redirect('/?nav=tasks')


@app.route('/task/delete/<int:id>')
def delete_task(id):
    """
    get id of the task to delete
    if id is not valid
        404
    delete from db
    commit to db
    redirect to index with nav
    """
    task_to_delete = Todo.query.get_or_404(id)
    try:
        db.session.delete(task_to_delete)
        db.session.commit()
    except:
        log_error('could not remove task from db ', 'Aufgabe wurde nicht entfernt!', should_flash=True)
    return redirect('/?nav=tasks')


@app.route('/task/update/<int:id>', methods=['GET', 'POST'])
def update_task(id):
    """
    get id of the task to update
    if id is not valid
        back to task list

    if request method is POST
        update the task
        redirect to index with nav: tasks

    if request method is GET
        render update_task.html with
    """
    task = Todo.query.get_or_404(id)
    if request.method == 'POST':
        task.content = request.form['content']
        try:
            db.session.commit()
        except:
            log_error('could not update task in db ', 'Aufgabe wurde nicht bearbeitet!', should_flash=True)
        return redirect('/?nav=tasks')
    return render_template('update_task.html', task=task)


backupbox_fragen_leer = {
    0: {
        "frage": "Bist du mit deiner aktuellen Backup-Strategie zufrieden?",
        "antwort": "",
        "A": "Ja",
        "B": "Nein",
        "C": "Habe keine konkrete"
    },
    1: {
        "frage": "Was ist deine aktuelle Backup-Strategie?",
        "antwort": "",
        "A": "Keine",
        "B": "Externe Festplatte / NAS",
        "C": "Google Cloud, MicroSoft OneDrive, Apple iCloud, usw"
    },
    2: {
        "frage": "Kennst du deine Dateitypen von denen du ein Backup machen möchtest, wenn ja was sind diese? (z.B. PDF, jpg, mp3, doc)",
        "antwort": "",
        "A": "Ja",
        "B": "Nein",
        "C": "Teils"
    },
    3: {
        "frage": "Ist dir bewusst wie hoch dein Speicherbedarf ist?",
        "antwort": "",
        "A": "Ja",
        "B": "Nein",
        "C": "Ungefähr"
    },
    4: {
        "frage": "Wie viel Speicherplatz benötigst du? (Geschätzt in GB, ein GB sind ca. 100 hoch auflösende Bilder)",
        "antwort": "",
        "A": "500 GB",
        "B": "1'000 GB",
        "C": "Mehr als 2'000 GB"
    },
    5: {
        "frage": "Gab es einmal eine Situation in der ein Backup von nutzen gewesen wäre/war?",
        "antwort": "",
        "A": "Ja",
        "B": "Nein",
        "C": "Leider, ja"
    },
    6: {
        "frage": "Traust du dir zu selber für deine Backups verantwortlich zu sein?",
        "antwort": "",
        "A": "Ja",
        "B": "Nein",
        "C": "Unter Umständen"
    },
    7: {
        "frage": "Ich baue eine mit dem Internet verbundene Box für zuhause, namens BackupBox. Welche Backups für dich verwaltet. Wie viel bist du bereit für eine BackupBox zu zahlen?",
        "antwort": "",
        "A": "200 CHF",
        "B": "400 CHF",
        "C": "500 CHF"
    },
    8: {
        "frage": "Wie lange sollte die BackupBox, deiner Meinung nach, mindestens nutzbar sein?",
        "antwort": "",
        "A": "3 Jahre",
        "B": "5 Jahre",
        "C": "Dumme Frage"
    },
    9: {
        "frage": "Für die haupsächliche Interaktion, mit der BackupBox, ist für dich eine Handy Applikation oder ein Computer Programm wichtiger?",
        "antwort": "",
        "A": "Handy",
        "B": "Computer",
        "C": "Beides"
    },
    10: {
        "frage": "Wie oft soll ein Backup deiner Bilder und Dokumente gemacht werden?",
        "antwort": "",
        "A": "Bei Änderungen",
        "B": "Täglich",
        "C": "Wöchentlich"
    },
    11: {
        "frage": "Hättest du gerne ein zweites Backup zu dem auf der BackupBox, welches nicht bei dir Zuhause ist?",
        "antwort": "",
        "A": "Nein",
        "B": "Bei einem Freund/Familienmitglied",
        "C": "Bei einem Online Dienst"
    },
    12: {
        "frage": "Wie gross sollte eine BackupBox sein?",
        "antwort": "",
        "A": "So wie ein WLAN Router",
        "B": "Schuhschachtel",
        "C": "So klein wie möglich"
    },
    13: {
        "frage": "Traust du dir zu selber mehr Speicher zu verbauen? (Bediehnungsanleitung, Teile und Werkzeug werden bereitgestellt)",
        "antwort": "",
        "A": "Ja",
        "B": "Nein",
        "C": "Muss die BackupBox erst sehen"
    }
}


@app.route('/backupbox_fragen_init/token=<token>&name=<name>&vorname=<vorname>')
def backupbox_fragen_init(token, name, vorname):
    """
    compare token
    initialize a new user
    create url with a hash created from user info
    returns the custom url for the provided user
    """

    if token != 'testing':  # ToDo: replace me in production
        abort(404)

    h = blake2b(digest_size=20)
    items = [
        bytes(name, encoding='utf8'),
        bytes(vorname, encoding='utf8'),
        bytes(token, encoding='utf8'),
        bytes(str(datetime.now().microsecond), encoding='utf8')
    ]
    for item in items:
        h.update(item)

    user_hash = h.hexdigest()
    new_user = BackupBoxUmfrage(
        user_hash=user_hash,
        name=name,
        vorname=vorname,
        antworten_json=""
    )
    try:
        db.session.add(new_user)
        db.session.commit()
    except Exception as e:
        log_error('backupbox_fragen commit to db failed', 'backupbox_fragen commit to db failed', should_flash=False)
        print(e)
        abort(404)

    return f"""
    url for {vorname} {name} is: "http://bonventre.ch/backupbox_fragen/{user_hash}"
    """


@app.route('/backupbox_fragen/<user_hash>', methods=['GET'])
def backupbox_fragen(user_hash):
    """
    compare id with db
        get values from db for user
        return values
    """
    user = None
    try:
        user = BackupBoxUmfrage.query.filter_by(user_hash=user_hash).first_or_404()
    except Exception as e:
        log_error('backupbox_fragen lookup db failed', 'backupbox_fragen lookup db failed', should_flash=False)
        print(e)
        abort(404)

    return render_template(
        'backupbox_fragen.html',
        fragen=backupbox_fragen_leer,
        name=user.name.capitalize(),
        vorname=user.vorname.capitalize(),
        user_hash=user_hash
    )


@app.route('/backupbox_antworten/<user_hash>', methods=['POST'])
def backupbox_antworten(user_hash):
    if request.method == 'POST':
        valid_responses: dict = {}
        counter = 0
        for entry in request.form:
            if str(counter - 1) in entry:
                continue
            if request.form[entry] != "":
                valid_responses[counter] = request.form[entry]
                counter = counter + 1

        """
        for antwort_key in valid_responses:
            fragen_und_antworten[antwort_key]["antwort"] = valid_responses[antwort_key]
        """

        user = BackupBoxUmfrage.query.filter_by(user_hash=user_hash).first_or_404()
        user.antworten_json = json.dumps(valid_responses)
        try:
            db.session.commit()
            log_error('Danke für deine Antworten!', 'Danke für deine Antworten!', should_flash=True)
        except Exception as e:
            log_error('could not update bb_fragen in db', 'BackupBox fragen wurde nicht bearbeitet!', should_flash=True)
            print(e)
    return redirect('/')


# LinkTracker / LinkShortener
@app.route('/test')
def test():
    new_task = Todo(content='test')
    db.session.add(new_task)
    db.session.commit()

    # return render_template('video.html')
    return redirect("bonventre.ch", code=302)


if __name__ == "__main__":
    app.run(debug=True)

